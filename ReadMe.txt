
TUTORIAL
========

	'code-first' approach on how to create a CXF REST clients and servers
	(from scratch, using a text editor, all based on Fuse 6.2.1)

	Index:
	-----
		1. Implementation and Deployment in Fuse
		2. Running standalone (and testing)
		3. Alternative CXF implementations
			> no 'rsServer' endpoint
			> use of component 'cxfbean'
			  - REST over HTTP
			  - REST over JMS
		4. Defining multiple services

		Note:
			For REST-Client definitions, refer to the 'rest-cxf-wadl-first' tutorial.


	And learn interesting bits like:
	--------------------------------
		- display registered services in Fuse
		- decouple the service from the transport
		- how to trigger Camel routes within a bean
		- test easily with SoapUI
		- embed an ActiveMQ broker for testing 


1. Implementation and Deployment
   (and testing using SoapUI)
=================================

1.1 Create skeleton

	mvn archetype:generate                   \
	  -DarchetypeGroupId=org.apache.camel.archetypes  \
	  -DarchetypeArtifactId=camel-archetype-blueprint \
	  -DarchetypeVersion=2.15.1.redhat-621084  \
	  -DgroupId=learn.rest                  \
	  -DartifactId=rest-cxf-cf

	(there is a bug in the JUnit, remove the slash in "/OSGI-INF...")

	Note: the archetype will generate some code that may enter in conflict
		  with our tutorial, simply remove the non-relevant code.

1.2 Add the endpoint in Blueprint

	a. Add the namespace:  
		xmlns:cxf="http://camel.apache.org/schema/blueprint/cxf"

	b. and the <cxf:rsServer> endpoint:

		<cxf:rsServer id="mycxfrest"
			 address="http://localhost:10000/say"
			 serviceClass="learn.rest.Hello"/>

		Notes about 'address':
			- it requires 'host:port' if run standalone (when testing or with 'camel:run')
			- when deployed in Fuse, you can omit 'host:port'
				e.g.
				 > address="/say" 
			    and it will default to:
				 > http://localhost:8181/cxf/say


1.3 Define the service's interface 

	The interface 'learn.rest.Hello' would look as follows:

		----------------------------------
		import javax.ws.rs.*;
		@Path("hello")
		public interface Hello {
			@GET
			@Path("{queryString}")
			String hello(@PathParam("queryString") String queryString);
		}
		----------------------------------

	Note: the interface alone is enough, there is no need to define an implementation class.


1.4 Add the Camel CXF route to consume traffic

		<route id="cxf-rest-1">
		  <from uri="cxfrs:bean:mycxfrest?bindingStyle=SimpleConsumer"/>
		  <setBody>
		    <simple>Hello ${header.queryString}</simple>
		  </setBody>
		</route>

		Note: CXF Rest consumers are defined using the 'cxfrs' component
		      (see 'Alternative CXF implementation' for a different type of consumer)


1.5 Add the Camel CXF dedendency in the POM file

			...
			<camel.version>2.15.1.redhat-621084</camel.version>
			<cxf.version>3.0.4.redhat-621084</cxf.version>
		</properties>
		...
		<dependency>
			<groupId>org.apache.camel</groupId>
			<artifactId>camel-cxf</artifactId>
			<version>${camel.version}</version>
		</dependency>


1.6 Install in Maven and deploy in Fuse (and start)

	a. mvn clean install -DskipTests=true

	b. JBossFuse:karaf@root> install -s mvn:learn.rest/rest-cxf-cf/1.0.0


1.7 From a browser, check it got deployed:

	With the address:
	http://localhost:8181/cxf

	It should show:

	Available RESTful services:
	  Endpoint address: http://localhost:10000/say
	  WADL : http://localhost:10000/say?_wadl


1.8 From SoapUI, you should be able to test it:

	a. Select: File > New REST Project
	b. Press the button 'Import WADL...' and enter:
		> http://localhost:10000/say?_wadl
	c. In the 'resource' textbox replace the value '{queryString}'
	d. trigger the request and you should get the response back.



2. Testing JUnits and running standalone
========================================

	JBoss Fuse (the engine) already contains all the dependencies required by the project.
	But the project won't run without a number of dependencies when:
		a. running JUnits (maven test phase)
		b. running standalone (with 'mvn camel:run')


2.1 The POM file needs some CXF dependencies:

	a. add dependency ASM at the top of <dependencies>

		<dependency>
		  <groupId>org.ow2.asm</groupId>
		  <artifactId>asm-all</artifactId>
		  <version>4.1</version>
		</dependency>

		(ref: https://access.redhat.com/solutions/1128593)

	b. add some extra CXF dependencies:

		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http</artifactId>
		    <version>${cxf.version}</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http-jetty</artifactId>
		    <version>${cxf.version}</version>
		</dependency>

		(ref: http://cxf.apache.org/docs/using-cxf-with-maven.html)

2.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:10000/say/hello/{queryString}
	   e.g.
		> http://localhost:10000/say/hello/world

	Note: there seems to be a bug as the WADL seems not available via:
		> http://localhost:10000/say?_wadl


2.3 To run Junits

	a. add the following JUnit that uses the CXF endpoint to send a WS request:

			@Test
			public void testRestCxf() throws Exception{
				String uri = "netty-http:http://localhost:10000/say/hello/junit";
				String response = template.requestBody(uri, null, String.class);
				assertEquals("ups", "Hello junit", response);
			}

		Note: the body sent is 'null', this ensures Netty uses the HTTP GET method on the request.

	   The Junit makes use of Camel Netty HTTP, therefore the following Maven dependendy needs to be added:

			<dependency>
			  <groupId>org.apache.camel</groupId>
			  <artifactId>camel-netty-http</artifactId>
			  <version>${camel.version}</version>
			</dependency>

	b. run the maven command:
		> mvn clean test



3. Alternative implementations for the CXF REST consumer
========================================================

	Three presented alternatives:
		a) no <cxf:rsServer> definition.
		b) REST over Netty HTTP using 'cxfbean' instead of 'cxfrs'.
		c) REST over  ActiveMQ  using 'cxfbean' instead of 'cxfrs'.


A) No 'rsServer' endpoint definition.

	In Camel you can define the consumer without referencing any 'rsServer' endpoint.


  a.1 Implementation

	Add the following route definition:

		<route id="cxf-rest-2">
		  <from uri="cxfrs:http://localhost:20000/say?bindingStyle=SimpleConsumer&amp;resourceClasses=learn.rest.Hello"/>
		  <setBody>
		    <simple>Hello ${header.queryString}</simple>
		  </setBody>
		</route>


  a.2 Install in Maven and redeploy in Fuse.
      From a browser, check it got deployed:

	With the address:
	http://localhost:8181/cxf

	It should show:

	Available RESTful services:
	  Endpoint address: http://localhost:10000/say
	  WADL : http://localhost:10000/say?_wadl
	  --------------------------------------------
	  Endpoint address: http://localhost:20000/say
	  WADL : http://localhost:20000/say?_wadl


  a.3 From SoapUI, you should be able to test it:

	a. Select: File > New REST Project
	b. Press the button 'Import WADL...' and enter:
		> http://localhost:20000/say?_wadl
	c. In the 'resource' textbox replace the value '{queryString}'
	d. trigger the request and you should get the response back.


B) (REST over HTTP) Using the 'cxfbean' component.

	It might be very interesting to decouple the CXF processing from the transport layer.
	Some reasons why this might be useful:
		> gain more control over the HTTP layer
		> use other transports other than HTTP


  b.1 Implementation

	Add the following route definition:

		<route id="cxf-rest-over-netty-http">
		  <from uri="netty-http:http://localhost:30000?matchOnUriPrefix=true"/>
		  <to uri="cxfbean:hellobean"/>
		</route>


	where 'hellobean' is defined in Blueprint as

		<bean id="hellobean" class="learn.rest.HelloBean"/>


	and where 'HelloBean' would be defined as

		public class HelloBean implements Hello {
			public String hello(String queryString) {
				return "Hello " + queryString;
			}
		}


  b.2 Install in Maven and redeploy in Fuse.

	  Note: this implementation appears to hide from CXF's registry, and the service won't show from:
		  > http://localhost:8181/cxf


  b.3 From SoapUI, you should be able to test it:

	a. Select: File > New REST Project
	b. Press the button 'Import WADL...' and enter:
		> http://localhost:30000/?_wadl
	c. In the 'resource' textbox replace the value '{queryString}'
	d. trigger the request and you should get the response back.


  (extra)
  b.4 Trigger Camel routes from within the cxfbean 

	The 'cxfbean' component relies on the Java Bean to implement the logic, unlike 'cxf' and 'cxfrs'.
	But this could be a limitation if the service requires more complex integration to be resolved.
	For elaborated integration use cases using 'cxfbean', we could extend the logic as follows:

	Redefine 'HelloBean' as:

		public class HelloBean implements Hello {
			@EndpointInject(uri="direct:cxfbean-route")
			ProducerTemplate producer;

			public String hello(String queryString) {
				String response = producer.requestBody((Object)queryString, String.class);
				return response;
			}
		}

    By injecting an endpoint to the Bean, we allow it to trigger Camel routes to resolve the integration calls.


  b.5 and include the route definition 'direct:cxfbean-route'

		<route id="cxfbean-route">
		  <from uri="direct:cxfbean-route"/>
		  <setBody>
		    <simple>Hello ${body}</simple>
		  </setBody>
		</route>

	What is being demonstrated here is how to trigger Camel routes from 'cxfbean' if needed.
    These triggers could help to resolve complicated integration use cases, obvioulsly the sample above was very simple.


C) (REST over JMS) Using the 'cxfbean' component via ActiveMQ.

	This example shows how to decouple the CXF processing from the transport layer.
	The transport used will be JMS (ActiveMQ) instead of HTTP.
	The service to invoke is the same as in the REST-over-HTTP example.


  c.1 Implementation

	Add the following route definition:

		<route id="cxf-rest-over-activemq-jms">
		  <from uri="activemq:hello.queue"/>
		  <to uri="cxfbean:hellobean"/>
		</route>

	Note the traffic gets consumed from a JMS queue 'hello.queue' using the component 'activemq'.

	where 'hellobean' is defined in Blueprint as

		<bean id="hellobean" class="learn.rest.HelloBean"/>


  c.2 A JUnit will used to test the JMS REST service above defined

	Add the following JUnit definition:

		@Test
		public void testRestCxfAlternativeC() throws Exception{
			Map<String, Object> headers = new HashMap<>();
			headers.put("CamelHttpUri",   "/hello/junit");
			headers.put("CamelHttpPath",  "/hello/junit");
			headers.put("CamelHttpMethod","GET");

			String response = template.requestBodyAndHeaders(
				"activemq:hello.queue",
				null, 
				headers, 
				String.class);

			assertEquals("ups", "Hello junit", response);
		}

	  Note how the JUnit maps the headers 'cxfbean' needs in order to resolve the call.
	  The headers are mapped in ActiveMQ as JMS properties in the JMS request message.
	  The JMS consumer maps automatically the JMS properties back to headers.


  c.3 The testing framework requires an embeded ActiveMQ broker for the JUnit to succeed.

	Include the following methods in the JUnit class

	(ref: https://github.com/joelicious/eip-example-polling-service/blob/master/service.polling.ftplistener/src/test/java/org/jboss/fuse/service/polling/ftplistener/FTPListenerTest.java)

		@Override
		@Before
		public void setUp() throws Exception {
			broker = new BrokerService();
			TransportConnector connector = new TransportConnector();
			connector.setUri(new java.net.URI("tcp://localhost:61616"));
			broker.setBrokerName("activemq");
			broker.addConnector(connector);
			broker.setPersistent(false);
			broker.start();
			super.setUp();
		}

		@Override
		@After
		public void tearDown() throws Exception {
			super.tearDown();
			broker.stop();
			broker = null;
		}

	  Note the broker is configured with the default URI the Camel ActiveMQ component uses.


	c.4 add the Maven ActiveMQ dependency

		<dependency>
		  <groupId>org.apache.activemq</groupId>
		  <artifactId>activemq-camel</artifactId>
		  <version>5.11.0.redhat-621084</version>
		</dependency>

	  Note the ActiveMQ version matches the one deployed in Fuse 6.2.1


	c.5 Now the route is ready to be tested running the command:

		> mvn clean test -Dtest=RouteTest#testRestCxfAlternativeC



4. Multiple REST service definitions
====================================

	This section shows how to include multiple REST interfaces under the same endpoint.
	The sample below exposes 2 interfaces (Hello,Bonjour) under the same REST endpoint.

4.1 Define a new Rest Interface

	The interface 'learn.rest.Bonjour' would look as follows:

		----------------------------------
		import javax.ws.rs.*;
		@Path("bonjour")
		public interface Bonjour {
			@GET
			@Path("{queryString}")
			String bonjour(@PathParam("queryString") String queryString);
		}
		----------------------------------


4.2 Add a new Camel CXF route to consume traffic

		<route id="cxf-rest-4">
		  <from uri="cxfrs:http://localhost:40000/multilingual?bindingStyle=SimpleConsumer&amp;resourceClasses=learn.rest.Hello,learn.rest.Bonjour"/>
		  <setBody>
		    <simple>${header.operationName} ${header.queryString}</simple>
		  </setBody>
		</route>

	Note:
	  - the attribute 'resourceClasses' includes both Hello and Bonjour interfaces
	  - the header 'operationName' is populated by CXF and specifies the REST operation in action.


4.3 Include Testing

	The following Junit should prove both services work as expected:

		@Test
		public void testRestMultipleInterfaces() throws Exception{
			String uri = "netty-http:http://localhost:40000/multilingual";
			String response;

			response = template.requestBody(uri+"/hello/junit", null, String.class);
			assertEquals("ups", "hello junit", response);

			response = template.requestBody(uri+"/bonjour/junit", null, String.class);
			assertEquals("ups", "bonjour junit", response);
		}


4.4 Rebuild and redeploy in Fuse.

    From a browser, check it got deployed:

	With the address:
	http://localhost:8181/cxf

	It should now also include:

	Available RESTful services:
	  Endpoint address: http://localhost:40000/multilingual
	  WADL : http://localhost:40000/multilingual?_wadl


4.5 From SoapUI, you should be able to test it:

	a. Select: File > New REST Project
	b. Press the button 'Import WADL...' and enter:
		> http://localhost:40000/multilingual?_wadl
	   Two nodes, 'hello' and 'bonjour' should appear under the project tree
	c. In the 'resource' textbox replace the value '{queryString}'
	d. trigger the request and you should get the response back.



