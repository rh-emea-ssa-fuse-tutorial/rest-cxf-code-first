package learn.rest;

import javax.ws.rs.*;

@Path("bonjour")
public interface Bonjour {

    @GET
    @Path("{queryString}")
    String bonjour(@PathParam("queryString") String queryString);
	
}
