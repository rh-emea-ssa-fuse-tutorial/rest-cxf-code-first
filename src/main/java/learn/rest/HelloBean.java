package learn.rest;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;

public class HelloBean implements Hello {

	@EndpointInject(uri="direct:cxfbean-route")
	ProducerTemplate producer;

    public String hello(String queryString) {
		String response = producer.requestBody((Object)queryString, String.class);
		return response;
    }
}
