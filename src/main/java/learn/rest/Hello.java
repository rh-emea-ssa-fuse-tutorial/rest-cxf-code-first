package learn.rest;

import javax.ws.rs.*;
/**
 * An interface for implementing Hello services.
 */
@Path("hello")
public interface Hello {

    @GET
    @Path("{queryString}")
    String hello(@PathParam("queryString") String queryString);
	
}
