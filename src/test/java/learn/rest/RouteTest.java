package learn.rest;


import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;

import java.util.Map;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RouteTest extends CamelBlueprintTestSupport {
	
    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

	//ActiceMQ broker
	protected static BrokerService broker;


	@Override
	@Before
	public void setUp() throws Exception {
		// Embed a JMS Broker for testing
		broker = new BrokerService();
		TransportConnector connector = new TransportConnector();
		connector.setUri(new java.net.URI("tcp://localhost:61616"));
		broker.setBrokerName("activemq");
		broker.addConnector(connector);
		broker.setPersistent(false);
		broker.start();

		// Creates the Blueprint Context
		super.setUp();
	}

	@Override
	@After
	public void tearDown() throws Exception {
		super.tearDown();
		broker.stop();
		broker = null;
	}

	@Test
	public void testRestCxf() throws Exception{
		String uri = "netty-http:http://localhost:10000/say/hello/junit";
		String response = template.requestBody(uri, null, String.class);
		assertEquals("ups", "Hello junit", response);
	}

	@Test
	public void testRestCxfAlternativeA() throws Exception{
		String uri = "netty-http:http://localhost:20000/say/hello/junit";
		String response = template.requestBody(uri, null, String.class);
		assertEquals("ups", "Hello junit", response);
	}

	@Test
	public void testRestCxfAlternativeB() throws Exception{
		String uri = "netty-http:http://localhost:30000/hello/junit";
		String response = template.requestBody(uri, null, String.class);
		assertEquals("ups", "Hello junit", response);
	}

	@Test
	public void testRestCxfAlternativeC() throws Exception{
		Map<String, Object> headers = new HashMap<>();
		headers.put("CamelHttpUri",   "/hello/junit");
		headers.put("CamelHttpPath",  "/hello/junit");
		headers.put("CamelHttpMethod","GET");

		String response = template.requestBodyAndHeaders(
			"activemq:hello.queue",
			null, 
			headers, 
			String.class);

		assertEquals("ups", "Hello junit", response);
	}

	@Test
	public void testRestMultipleInterfaces() throws Exception{
		String uri = "netty-http:http://localhost:40000/multilingual";
		String response;

		response = template.requestBody(uri+"/hello/junit", null, String.class);
		assertEquals("ups", "hello junit", response);

		response = template.requestBody(uri+"/bonjour/junit", null, String.class);
		assertEquals("ups", "bonjour junit", response);
	}
}
